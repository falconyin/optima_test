<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;

            }

            ul {
                width: 300px;
                list-style-type: none;
                margin-left: 200px;
                padding: 0;
                overflow: hidden;
            }
            li {
                float: left;
                padding: 5px 10px;
            }
            table {
                margin-left: 100px;
            }
            td.number {
                text-align: right;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <table>
                    <thead>
                        <tr>
                            <th>Product Name</th>
                            <th>Total Available</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($products as $product)
                            <tr>
                                <td>{{ $product->product_name }}</td>
                                <td class="number">{{ $product->total_available }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                {{ $products->links() }}
            </div>
        </div>
    </body>
</html>
