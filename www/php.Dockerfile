FROM php:7.4-fpm
RUN apt-get update
RUN apt-get install -y autoconf pkg-config libssl-dev git unzip
RUN pecl install mongodb
RUN echo "extension=mongodb.so" >> /usr/local/etc/php/conf.d/mongodb.ini
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

COPY ./www /var/www/html
WORKDIR /var/www/html
COPY ./www/.env-docker .env
RUN chown -R www-data:www-data storage

RUN composer install
