<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Product;

class ProductController extends Controller
{
    /**
     * Show the list of products.
     *
     * @return View
     */
    public function show()
    {
        // Get product list from MongoDB, 10 products per page
        $products = Product::orderBy('total_available', 'desc')->paginate(10);
        return view('product', ['products' => $products]);
    }
}