<?php
namespace App;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

/**
 * Class Product
 * @property string $product_name
 * @property int64 $total_available
 */
class Product extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'optima_test';
    protected static $unguarded = true;
    protected $primaryKey = 'product_name';

}