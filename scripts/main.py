import logging
import argparse
import os

from crawler import Crawler

parser = argparse.ArgumentParser()
parser.add_argument('-f', '--file', help='config file')
parser.add_argument('--url', default='https://api.rrpcanada.org/products', help='API url to fetch data from')
parser.add_argument('-H', '--host', default='ds029630.mlab.com', help='host name of MongoDB')
parser.add_argument('-p', '--port', default='29630', help='port number of MongoDB', type=int)
parser.add_argument('-u', '--user', default='jun', help='user name of MongoDB')
parser.add_argument('-P', '--pass', default='jun_test_2020', dest='pwd', help='password of MongoDB')
parser.add_argument('-d', '--db', default='falcon_dev', help='database name of MongoDB')
parser.add_argument('-c', '--col', default='optima_test', help='collection name of MongoDB')
parser.add_argument('-m', '--min', default='60', help='minimal interval (in seconds) between each try of fetching data from API', type=int)
parser.add_argument('-M', '--max', default='3600', help='maximal interval (in seconds) between each try of fetching data from API', type=int)

args = parser.parse_args()

# Initialize crawler object with different arguments.
if args.file:
    worker = Crawler(config_file=("%s/%s" % (os.getcwd(), args.file)))
else:
    worker = Crawler(args=args)

worker.run()

# TODO:
# 1. Delete items not in api response
# 2. Change sleep time automatically
# 3. Add create time and update time
# 4. Add monitor and alert





