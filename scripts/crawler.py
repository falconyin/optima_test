import json
import logging
import os
import time
import urllib
from datetime import datetime
from json import JSONDecodeError
from os.path import exists
from urllib.parse import quote_plus
from urllib.request import urlopen
from urllib.error import HTTPError
from pathlib import Path


from bson import Int64
from pymongo import MongoClient
from pymongo.errors import ConnectionFailure


class Crawler:

    flag_run = True
    wait_interval = 0

    def __init__(self, config_file='config.json', args=None):
        logging.basicConfig(level=logging.DEBUG)

        # Initialize Crawler with information from config file or command line
        if args and args.host and args.port and args.user and args.pwd and args.db:
            self.mongo_host = args.host
            self.mongo_port = args.port
            self.mongo_user = args.user
            self.mongo_pass = args.pwd
            self.mongo_db = args.db
            self.mongo_collection = args.col
            self.api_url = args.url
            self.min_wait = args.min
            self.max_wait = args.max
        else:
            self.config_file = config_file
            with open(self.config_file) as json_config_file:
                try:
                    config = json.load(json_config_file)
                    self.mongo_host = config['mongo_host']
                    self.mongo_port = config['mongo_port']
                    self.mongo_user = config['mongo_user']
                    self.mongo_pass = config['mongo_pass']
                    self.mongo_db = config['mongo_db']
                    self.mongo_collection = config['mongo_collection']
                    self.api_url = config['api_url']
                    self.min_wait = config['min_wait']
                    self.max_wait = config['max_wait']
                except JSONDecodeError as err:
                    logging.error("Failed to load config file: {0}".format(err))

        self.headers = {
            "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36",
            "content-type": "application/json",
            "Origin": "https://www.rrpcanada.org",
            "Referer": "https://www.rrpcanada.org/"
        }
        try:
            # Connect to MongoDB instance
            self.client = MongoClient("mongodb://%s:%s@%s:%s/%s?retryWrites=false" % (
                quote_plus(self.mongo_user), quote_plus(self.mongo_pass), self.mongo_host, self.mongo_port, self.mongo_db))
            self.db = self.client[self.mongo_db]
            self.collection = self.db[self.mongo_collection]
        except ConnectionFailure as err:
            logging.error("Failed to connect to mongodb: {0}".format(err))

    def run(self):
        # Set interval to minimal in config
        self.wait_interval = self.min_wait

        while self.flag_run:
            updated = False
            json_data = self.get_data_from_api()

            items = []
            if json_data['status'] and json_data['status'] == 'OK' and 'content' in json_data and 'content' in json_data['content']:
                try:
                    for item in json_data['content']['content']:
                        updated = True if self.save_data(item) else updated
                        items.append(item['description'])
                    updated = True if self.clear_data(items) > 0 else updated
                except KeyError as err:
                    logging.error("Key: '{0}' not found".format(err))
                    exit(1)
            else:
                logging.warning("Data from API is not processed")

            self.calculate_new_interval(updated)
            self.check_stop_sign()
            time.sleep(self.wait_interval)

    def get_data_from_api(self):
        """Get data from API"""
        ret = {}
        try:
            request = urllib.request.Request(self.api_url, headers=self.headers)
            response = urllib.request.urlopen(request)
            ret = json.loads(response.read())
        except HTTPError as err:
            logging.error("Failed to access api: {0}".format(err))
        except JSONDecodeError as err:
            logging.error("Failed to extract JSON data: {0}".format(err))
        finally:
            return ret

    def save_data(self, data):
        """Save document to MongoDB and return if it is new or updated"""
        updated = False
        now = datetime.now()
        condition = {'product_name': data['description']}
        new_data = {'id': data['id'], 'product_name': data['description'],
                    'total_available': Int64(data['aggregateDemandSupply']), 'updated_at': now}
        orig_doc = self.collection.find_one(condition)
        if orig_doc is None or orig_doc['total_available'] != new_data['total_available']:
            updated = True

        result = self.collection.update_one(
            condition,
            {"$set": new_data, "$setOnInsert": {"created_at": now}},
            upsert=True)
        logging.info("Updated: %s,%d  Result: [acknowledged: %s, matched_count: %d, modified_count: %d, upserted_id: "
                     "%s]" % (new_data['product_name'], new_data['total_available'],
                              result.acknowledged, result.matched_count, result.modified_count, result.upserted_id))
        return updated

    def clear_data(self, keys_to_keep=None):
        """Delete documents not in api response"""
        if keys_to_keep is None:
            keys_to_keep = []
        ret = self.collection.delete_many({"product_name": {"$nin": keys_to_keep}})
        return ret.deleted_count

    def check_stop_sign(self):
        """If there is a file named 'stop' in the same directory of crawler.py, stop the script."""
        if exists("%s/stop" % Path(__file__).resolve().parent):
            logging.info("Found stop file, will stop now.")
            self.flag_run = False
            os.remove('stop')
            exit(0)

    def calculate_new_interval(self, updated=False):
        """Update interval based on if any record changed in last run"""
        if updated:
            # If any record changed in last run, reduce the interval by half
            self.wait_interval = round(self.wait_interval / 2)
        else:
            # If no record changed in last run, double the interval
            self.wait_interval *= 2

        # make sure the interval is between the minimal and maximal intervals from configuration
        self.wait_interval = self.min_wait if self.wait_interval < self.min_wait else self.wait_interval
        self.wait_interval = self.max_wait if self.wait_interval > self.max_wait else self.wait_interval
