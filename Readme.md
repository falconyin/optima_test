# Requirement
## python
1. python3
2. pymongo
## Web
1. php >= 7.2
2. php-mongodb extension
3. laravel 6.x
4. jenssegers/mongodb 3.6
5. composer

# Usage

## Docker
```shell script
docker-compose up -d
```
Then go to http://127.0.0.1:8080 to see the result

## Python script
```shell script
pip install -r requirements.txt
# specify config file
python ./scripts/main.py -f config-shell.json
# specify each setting
python ./scripts/main.py [--url URL] [-H HOST] [-p PORT] [-u USER]
               [-P PWD] [-d DB] [-c COL] [-m MIN] [-M MAX]
```
## Web
```shell script
composer install
php www/artisan serve
```
Then go to http://127.0.0.1:8080 to see the result


# How does this work

## Fetching data from website
At first I thought I need to get HTML markups from https://www.rrpcanada.org/, and then use regular expression to extract the data. But after a few minutes digging, it turns out that the data is provided by an API: https://api.rrpcanada.org/products. So this part became very straightforward: retrieve content using urllib, decode the response and save to a JSON object.

## Extract product name and total available qty
After the data been saved to a JSON object, it was not hard to find the keys related to product name and total available quantity.

## Save product name and total available to MongoDB
After the name and quantity is ready, I saved this info to MongoDB in following steps:
1. Check if there is record in DB has the same product name;
2. Insert a new record or update the existing record;
3. Remove products from DB if they are not in the API response.

## Show data in web page
I use package jenssegers/mongodb to access MongoDB from php.

A new model file (app/Product.php) was created to handle the data. 

A new view file (resources/views/product.blade.php) was created to show the table.

A new controller file (app/Http/Controllers/ProductController.php) was created to connect model and view.


# What did I do to control the speed of scraping hits
1. A minimal and a maximal interval are pass to the crawler, and the crawler runs with the minimal interval;
2. In each run, while saving data to MongoDB, a variable (updated) was set to show if anything changed in this run;
3. If something changed in this run, it may indicate that the data may change more frequently, so the crawler cut the interval by half unless it reaches the minimal value;
4. If nothing changed in this run, it may indicate that the data may not change frequently, so the crawler double the interval unless it reaches the maximal value;

# Some other ways to control the speed
1. Expose a management port to receive control signals;
2. Submit statics data to a central server and get speed configuration


# What can be done to make this program better

## Performance
In this particular case, the amount of data is quite small, so we can use only one python script running in single thread mode to finish the job. And when saving data to MongoDB, a query was made before each update. This could have substantial impact to performance if the data amount goes larger. 

For larger data, we can divide the functions in crawler script into different modules and run them in multi thread mode or act like spouts from Apache storm. We can also divide the data into smaller pieces by certain rules, and handle those pieces to modules to be processed. 

To reduce the frequency of access MongoDB, we can store key info in module's local cache, or use product like redis act as a high speed cache.

## Scalability
We can deploy the crawler module into containers and use orchestration product like kubernetes to make the management easily. Configuration management for these containerized programs could be a challenge, but I think Apache Zookeeper may save some effort.

## Reliability
Scripts may crash, especially for more complicated cases, so I think a monitoring system is necessary to make sure the whole system runs as expected.