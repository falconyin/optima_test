db.createUser(
    {
        user: "optima_test",
        pwd: "test_optima",
        roles: [
            {
                role: "readWrite",
                db: "optima_test"
            }
        ]
    }
);